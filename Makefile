CFLAGS := $(CFLAGS) -Wall

all32: peek poke
all64: peek poke peek64 poke64

poke: poke.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

peek: peek.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

poke64: poke.c
	$(CC) -DPEEKPOKE64 $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

peek64: peek.c
	$(CC) -DPEEKPOKE64 $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

clean:
	-rm -f peek poke peek64 poke64 *.rpm
