# Build this using apx-rpmbuild.
%define name peekpoke

Name:           %{name}
Version:        %{version_rpm_spec_version}
Release:        %{version_rpm_spec_release}%{?dist}
Summary:        A simple peek/poke command from Xilinx.

License:        Reserved
URL:            https://github.com/uwcms/APx-%{name}
Source0:        %{name}-%{version_rpm_spec_version}.tar.gz

%global debug_package %{nil}

%description
This package provides a simple peek/poke command from Xilinx.
Updated for aarch64 support.


%prep
%setup -q


%build
##configure
%ifarch aarch64
make %{?_smp_mflags} LIB_VERSION=%{version_sofile} all64
%else
make %{?_smp_mflags} LIB_VERSION=%{version_sofile} all32
%endif


%install
rm -rf $RPM_BUILD_ROOT
install -D -m 0755 peek %{buildroot}/%{_bindir}/peek
install -D -m 0755 poke %{buildroot}/%{_bindir}/poke
%ifarch aarch64
install -D -m 0755 peek64 %{buildroot}/%{_bindir}/peek64
install -D -m 0755 poke64 %{buildroot}/%{_bindir}/poke64
%endif

%files
%{_bindir}/peek
%{_bindir}/poke
%ifarch aarch64
%{_bindir}/peek64
%{_bindir}/poke64
%endif

%changelog
* Tue Jan 14 2020 Jesra Tikalsky <jtikalsky@hep.wisc.edu>
- Initial spec file
